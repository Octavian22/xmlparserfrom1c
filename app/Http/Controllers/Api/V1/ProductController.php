<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\V1;

use App\Actions\Product\ProductAction;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    public function parse(): JsonResponse
    {
        try {
            ProductAction::parseFrom1C();
        } catch (GuzzleException $exception) {
            Log::error($exception->getMessage());
            return response()->json([
                'message' => 'failed parse'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        return response()->json([
            'message' => 'success'
        ]);
    }
}
