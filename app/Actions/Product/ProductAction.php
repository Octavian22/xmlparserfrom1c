<?php

declare(strict_types=1);

namespace App\Actions\Product;

use App\Contracts\ParserFrom1C;
use App\Data\Product\ProductXmlDTO;
use App\Models\Product;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Facades\Log;

class ProductAction
{
    /**
     * @throws GuzzleException
     */
    public static function parseFrom1C(): void
    {
        $client = new Client();

        $tradeInMarkdown = (string) ($client->request(
            'GET',
            'https://cdn.iport.ru/files/xml/TradeIn_markdown.xml',
        ))->getBody();

        $products = ProductXmlDTO::fromResponse($tradeInMarkdown);
        foreach ($products as $product) {
            $productInfo = (string) ($client->request(
                'GET',
                "https://stage.api.iport.ru/api/products/{$product['product_code']}",
                ['http_errors' => false]
            ))->getBody();
            $productInfo = json_decode($productInfo, true);
            if ($productInfo['code'] === 200) {
                $product['name'] = $productInfo['data']['TITLE'];
                $product['images'] = json_encode($productInfo['data']['IMAGES']);
                $product['original_price'] = $productInfo['data']['PRICE']['VALUE'];
                $product['is_active'] = true;
            } else {
                $product['name'] = $product['product_code'];
                $product['is_active'] = false;
                $products = $products->whereNotIn('serial_number', $product['serial_number']);
            }

            Product::updateOrInsert(
                ['serial_number' => $product['serial_number']],
                [...$product],
            );
        }

        Product
            ::whereNotIn('product_code', $products->pluck('product_code'))
            ->update(['is_active' => false]);

        Log::info('End parsing products from 1C in ' . Carbon::now()->toDateTimeString());
    }
}
