<?php

declare(strict_types=1);

namespace App\Data\Product;

use Illuminate\Support\Collection;

class ProductXmlDTO
{
    public static function fromResponse(string $XmlProducts): Collection
    {
        $simpleXml = simplexml_load_string($XmlProducts);
        $products = json_decode(json_encode($simpleXml), true)['Товар'];
        $productCollect = collect();
        foreach ($products as $product) {
            if ($product['ЭтоТрейдИн'] === 'Да') {
                $productCollect->push([
                    'name' => '',
                    'serial_number' => !is_array($product['СерийныйНомер']) ? $product['СерийныйНомер'] : null,
                    'product_code' => (int) $product['КодТовара'],
                    'discounted_product_code' => (int) $product['КодУцененногоТовара'],
                    'warehouse' => !is_array($product['Склад']) ? $product['Склад'] : null,
                    'warehouse_name' => !is_array($product['СкладНаименование']) ? $product['СкладНаименование'] : null,
                    'region' => !is_array($product['Регион']) ? $product['Регион'] : null,
                    'price' => (float) $product['Цена'],
                    'engineer_comment' => !is_array($product['КомментарийИнженера']) ? $product['КомментарийИнженера'] : null,
                    'battery_is_new' => !is_array($product['АКБ_Новый']) ? filter_var($product['АКБ_Новый'], FILTER_VALIDATE_BOOLEAN) : null,
                    'package_contents_box' => !is_array($product['Комплектация_Коробка']) ? filter_var($product['Комплектация_Коробка'], FILTER_VALIDATE_BOOLEAN) : null,
                    'package_contents_cable' => !is_array($product['Комплектация_Кабель']) ? filter_var($product['Комплектация_Кабель'], FILTER_VALIDATE_BOOLEAN) : null,
                    'package_contents_adapter' => !is_array($product['Комплектация_Адаптер']) ? filter_var($product['Комплектация_Адаптер'], FILTER_VALIDATE_BOOLEAN) : null,
                    'package_contents_strap' => !is_array($product['Комплектация_Ремешок']) ? filter_var($product['Комплектация_Ремешок'], FILTER_VALIDATE_BOOLEAN) : null,
                    'reason_for_markdown' => !is_array($product['ПричинаУценкиРазвернуто']) ? $product['ПричинаУценкиРазвернуто'] : null,
                    'condition' => !is_array($product['Состояние']) ? $product['Состояние'] : null,
                ]);
            }
        }

        return $productCollect;
    }
}
