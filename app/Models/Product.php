<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'images',
        'original_price',
        'serial_number',
        'product_code',
        'discounted_product_code',
        'warehouse',
        'warehouse_name',
        'region',
        'price',
        'engineer_comment',
        'battery_is_new',
        'package_contents_box',
        'package_contents_cable',
        'package_contents_adapter',
        'package_contents_strap',
        'reason_for_markdown',
        'condition',
        'is_active',
    ];

    protected $casts = [
        'name' => 'string',
        'images' => 'json',
        'original_price' => 'float',
        'serial_number' => 'string',
        'product_code' => 'integer',
        'discounted_product_code' => 'integer',
        'warehouse' => 'string',
        'warehouse_name' => 'string',
        'region' => 'string',
        'price' => 'float',
        'engineer_comment' => 'string',
        'battery_is_new' => 'boolean',
        'package_contents_box' => 'boolean',
        'package_contents_cable' => 'boolean',
        'package_contents_adapter' => 'boolean',
        'package_contents_strap' => 'boolean',
        'reason_for_markdown' => 'string',
        'condition' => 'string',
        'is_active' => 'boolean',
    ];
}
