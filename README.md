# Тестовое задание на парсинг XML выгрузки с 1С

Стек:
- Laravel 10
- Php 8.1
- MySQL 8

---

Разворачивание проекта:
1.
```shell
composer install
```
2. 
```shell
php artisan key:generate
```
3.
```shell
php artisan migrate
```
---

Url к Api для парсинга товаров `http://localhost/api/v1/productParse`
