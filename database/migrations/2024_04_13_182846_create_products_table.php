<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('products', static function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->text('images')->nullable();
            $table->unsignedFloat('original_price')->nullable();

            $table->string('serial_number')->unique();
            $table->integer('product_code');
            $table->integer('discounted_product_code');
            $table->string('warehouse');
            $table->string('warehouse_name')->nullable();
            $table->string('region');
            $table->unsignedFloat('price');

            $table->text('engineer_comment')->nullable();
            $table->boolean('battery_is_new')->nullable();
            $table->boolean('package_contents_box')->nullable();
            $table->boolean('package_contents_cable')->nullable();
            $table->boolean('package_contents_adapter')->nullable();
            $table->boolean('package_contents_strap')->nullable();
            $table->text('reason_for_markdown')->nullable();
            $table->string('condition');

            $table->boolean('is_active');

            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
